---
title: About
leftcol: three
rightcol: nine
ribbon: bottom
---

<br>
<br>
![](bridge.png)


---

The goal of Lacre is to make itself obsolete! The more PGP becomes used on the internet, the less there is need for solutions like Lacre. If all email is end-to-end encrypted by default, using open interoperable standards, plain text emails should no longer have to be sent or stored on servers.


Lacre serves as a bridge between the current situation, where most of the email is transported and stored in plaintext, and a new situation where all emails are end-to-end encrypted using open standards.
