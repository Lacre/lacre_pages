---
title: About
leftcol: nine
rightcol: three
ribbon: left
---

### Lacre is an open source solution to email encryption.


Lacre provides a transparent multiplatform solution to secure all incoming emails. It is based on existing cryptographic standards.


By utilizing the well-known and established PGP protocol, it is compatible with many modern client softwares. This makes Lacre client software agnostic and vendor lock free.


---

<br>
<br>
![](lock.png)
