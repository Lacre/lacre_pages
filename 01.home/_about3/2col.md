---
title: About
leftcol: nine
rightcol: three
ribbon: left

---

Lacre is open and free!!! By providing an open source solution using existing standards, Lacre enables practically any email provider to offer protection of email at rest. Moreover, every new Lacre enabled mail server increases security of end-to-end email encryption in transit. This is achieved by harnessing the power of PGP, which allows you to encrypt email between PGP users, so that messages will remain encrypted on their way from sender to recipient, without the need for Lacre to do anything. The role of Lacre in this case is merely to help popularize open encryption standards.

---

<br>
<br>
![](hands.png)
