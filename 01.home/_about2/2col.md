---
title: About
leftcol: three
rightcol: nine
ribbon: right
---

<br>
<br>
![](plug.png)

---

For users: Lacre software provides end-to-end encryption of emails at rest (i.e. when it hits your mailbox) by not storing any private keys on the server. This makes it impossible to decrypt your data without the user’s key.

For admin: Lacre is pluggable. Working as a postfix filter, Lacre does not require an in-house SMTP server solution, but provides seamless integration into your existing infrastructure.
