---
title: How Does it Work
leftcol: zero
rightcol: twelve
ribbon: right
---

---

## How does it work?

From user perspective. In order to encrypt the mailbox, the user must generate a PGP encryption key pair. That set of keys consists of a private key used to decrypt emails and a public key, which is widely shared with anyone and it is used to encrypt emails addressed to the user. Lacre requires only the public key. This key is then used against all incoming emails addressed to the user to encrypt emails. Users can decrypt and read the emails only if they have has access to the private key. This means they must use a pgp enabled email client and needs to have their private key presents on the device. Without the key it is practically impossible to obtain the content of the email.

From server perspective, Lacre works as a postfix (SMTP server) filter. All incoming emails are passed to the filter that then checks whether a GPG key is present in the database for a given recipient. If a key is present, the email is being encrypted with the recipient's public key and transmitted to the IMAP server that then saves it on to the mailbox. If the incoming email is already encrypted or the recipient key is missing, Lacre does nothing and forwards the email to the IMAP server.
