---
title: How Does it Work
leftcol: twelve
rightcol: zero
ribbon: bottom
---

## What are known Issues and limitations?

There is no such thing as 100% safe and secure solution and anyone claiming otherwise is just laying. Lacre isn’t a "silver bullet" and does come with some drawbacks. While it does protect emails at rest (those that have been recieved received and encrypted with Lacre), it does not fully protect emails in transit. Unless email is end-to-end encrypted between both sender and recipient, there is a brief moment between when plain text email arrives to the server and when Lacre does its job to secure it. This applies to all encrypted email solutions outthere by the way. Even if they don’t tell you that. That means emails could potentially be intercepted in realtime. This is why trusting your mail service provider is important!

Lacre does not protect your metadata. Email is a service that exchanges a lot of metadata. Depending on your service provider, you might be sharing a lot of data with others including your IP address, mail client software used, time of sending, subject, recipient, etc. In order to keep to standards, not all of that information is and can be encrypted. Currently Lacre does take care of subject encryption and we are looking for possibilities to encrypt all possible metadata. Although from a software perspective this could be achieved by Lacre already, it all depends on email client software and what the gold standard is. We hope in the future that more and more metadata will be covered by Lacre.

Warning: GPG requires your private key to be able to decrypt emails. If you lose your key you lose access to the encrypted emails with that key. That means you won’t be able to decrypt emails that are already encrypted. Lacre does give you the possibility to reset your configuration on the server by removing the key or uploading a new one, but that change applies only to the future emails. This is also a drawback when it comes to forward secrecy, which Lacre odes does not provide. This means that if your key becomes compromised, all past communication using that key will be decryptable.
